const express = require('express');
const app = express();
const PORT = 3000;


/*Middleware*/
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//Homepage
app.get('/home', (req, res) => res.send(`Welcome to my home page`))

//users
const users = [{
    "username": "johndoe",
    "password": "johndoe123"
}]
app.get('/users', (req, res) => res.send(users));
 
//delete
app.delete('/delete-user', (req, res) => {
    res.send(`User ${req.body.username} has been deleted`)
})

app.listen(PORT, () => console.log(`Server running at port ${PORT}`))